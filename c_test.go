package casbin

import (
	"fmt"
	"testing"
)

type Ts struct {
	Type string `json:"type"`
	Sub  string `json:"sub"`
	Obj  string `json:"obj"`
	Act  string `json:"act"`
}

func TestA(t *testing.T) {
	t2 := &Ts{
		Type: "123",
		Sub:  "hhh",
		Obj:  "789",
		Act:  "dsa",
	}
	_map := StructToMap(*t2)
	fmt.Println(_map)
}
