package casbin

import (
	"fmt"
	"gitee.com/jqllxew/casbin/jwts"
	"gitee.com/jqllxew/simple"
	"gitee.com/jqllxew/simple/config"
	"github.com/casbin/casbin"
	_ "github.com/go-sql-driver/mysql"
	"github.com/kataras/iris/v12/context"
	"sync"
)

var (
	Adt       *Adapter
	e         *casbin.Enforcer
	adtLook   sync.Mutex
	eLook     sync.Mutex
	rbacModel string
)

func setRbacDefault(root string) {
	if root == "" {
		root = "admin"
	}
	rbacModel = fmt.Sprintf(`
[request_definition]
r = sub, obj, act

[policy_definition]
p = sub, obj, act

[role_definition]
g = _, _

[policy_effect]
e = some(where (p.eft == allow))

[matchers]
m = g(r.sub, p.sub) && keyMatch(r.obj, p.obj) && regexMatch(r.act, p.act) || r.sub == "%s"
`, root)
}
func setRbac(rbac string) {
	rbacModel = rbac
}

// 获取Enforcer
func getEnforcer() *casbin.Enforcer {
	if e != nil {
		_ = e.LoadPolicy()
		return e
	}
	eLook.Lock()
	defer eLook.Unlock()
	if e != nil {
		_ = e.LoadPolicy()
		return e
	}
	conf := config.GetSimple()
	if rbacModel == "" {
		setRbacDefault(conf.AuthRoot)
	}
	m := casbin.NewModel(rbacModel)
	e = casbin.NewEnforcer(m, singleAdapter())
	e.EnableLog(conf.ShowAuthLog)
	return e
}
func singleAdapter() *Adapter {
	if Adt != nil {
		return Adt
	}
	adtLook.Lock()
	defer adtLook.Unlock()
	if Adt != nil {
		return Adt
	}
	Adt = newAdapter()
	return Adt
}
func Permissions(ctx *context.Context, msg string) bool {
	if !jwts.Serve(ctx) {
		return false
	}
	_, info := jwts.ParseToken(ctx)
	e := getEnforcer()
	yes := e.Enforce(info, ctx.Path(), ctx.Method())
	if !yes {
		if msg == "" {
			msg = "您没有权限"
		}
		_, _ = ctx.JSON(simple.Unauthorized(msg, nil))
	}
	return yes
}
