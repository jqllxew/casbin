package casbin

import (
	"errors"
	"github.com/casbin/casbin/model"
	"github.com/casbin/casbin/persist"
	"github.com/kataras/golog"
	"reflect"
)

type (
	Adapter struct{}
)

var (
	Rules []interface{}
)

func newAdapter() *Adapter {
	a := &Adapter{}
	return a
}
func loadPolicyLine(line interface{}, model model.Model) {
	if line == nil {
		return
	}
	var lm map[string]string
	if lm = StructToMap(line); lm == nil {
		return
	}
	lineText := lm["type"]
	if lm["sub"] != "" {
		lineText += ", " + lm["sub"]
	}
	if lm["obj"] != "" {
		lineText += ", " + lm["obj"]
	}
	if lm["act"] != "" {
		lineText += ", " + lm["act"]
	}
	persist.LoadPolicyLine(lineText, model)
}
func (a *Adapter) LoadPolicy(model model.Model) error {
	if Rules == nil {
		return errors.New("rules is nil")
	}
	for _, rule := range Rules {
		loadPolicyLine(rule, model)
	}
	return nil
}
func (a *Adapter) SavePolicy(model model.Model) error {
	return nil
}
func (a *Adapter) AddPolicy(sec string, pType string, rule []string) error {
	return nil
}
func (a *Adapter) RemovePolicy(sec string, pType string, rule []string) error {
	return nil
}
func (a *Adapter) RemoveFilteredPolicy(sec string, pType string, fieldIndex int, fieldValues ...string) error {
	return nil
}
func StructToMap(obj interface{}) map[string]string {
	if reflect.ValueOf(obj).Type().Kind() != reflect.Struct {
		golog.Warn("obj is not struct")
		return nil
	}
	tf := reflect.TypeOf(obj)
	vf := reflect.ValueOf(obj)
	var data = make(map[string]string)
	for i := 0; i < tf.NumField(); i++ {
		field := tf.Field(i)
		keyName := field.Tag.Get("json")
		if keyName == "" {
			keyName = field.Name
		}
		data[keyName] = vf.Field(i).String()
	}
	return data
}
