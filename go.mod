module gitee.com/jqllxew/casbin

go 1.16

require (
	gitee.com/jqllxew/simple v0.2.92
	github.com/casbin/casbin v1.9.1
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-sql-driver/mysql v1.6.0
	github.com/kataras/golog v0.1.7
	github.com/kataras/iris/v12 v12.2.0-alpha2
)
