package jwts

import (
	"fmt"
	"gitee.com/jqllxew/simple"
	"gitee.com/jqllxew/simple/config"
	"github.com/dgrijalva/jwt-go"
	"github.com/kataras/iris/v12"
	"github.com/kataras/iris/v12/context"
	"log"
	"sync"
	"time"
)

type (
	errorHandler   func(*context.Context, string)
	TokenExtractor func(*context.Context) (string, error)
	Jwts           struct {
		Config Config
	}
)

var (
	jwts *Jwts
	lock sync.Mutex
)

// the middleware's action
func Serve(ctx *context.Context) bool {
	ConfigJWT()
	if err := jwts.CheckJWT(ctx); err != nil {
		return false
	}
	return true
}

// 解析token的信息为当前用户
func ParseToken(ctx *context.Context) (sign, info string) {
	mapClaims := (jwts.Get(ctx).Claims).(jwt.MapClaims)
	sign, ok := mapClaims["id"].(string)
	info, ok2 := mapClaims["info"].(string)
	if !ok || !ok2 {
		_, _ = ctx.JSON(simple.Unauthorized("token解析失败", nil))
		return "", ""
	}
	return
}

// below 3 method is get token from url
// FromAuthHeader is a "TokenExtractor" that takes a give context and extracts
// the JWT token from the Authorization header.
func FromAuthCookie(ctx *context.Context) (string, error) {
	cookie := ctx.GetCookie(conf().Simple.TokenKey)
	if cookie != "" {
		return cookie, nil // No error, just no token
	} else {
		header := ctx.GetHeader(conf().Simple.TokenKey)
		if header != "" {
			return header, nil
		}
	}
	return "", fmt.Errorf("请先登录")
}
func (m *Jwts) logf(format string, args ...interface{}) {
	if m.Config.Debug {
		log.Printf(format, args...)
	}
}

// returns the user (&token) information for this client/request
func (m *Jwts) Get(ctx *context.Context) *jwt.Token {
	return ctx.Values().Get(m.Config.ContextKey).(*jwt.Token)
}

// the main functionality, checks for token
func (m *Jwts) CheckJWT(ctx *context.Context) error {
	if !m.Config.EnableAuthOnOptions {
		if ctx.Method() == iris.MethodOptions {
			return nil
		}
	}
	// Use the specified token extractor to extract a token from the request
	token, err := m.Config.Extractor(ctx)
	// If an error occurs, call the error handler and return an error
	if err != nil {
		m.Config.ErrorHandler(ctx, err.Error())
		return err
	}
	// Now parse the token
	parsedToken, err := jwt.Parse(token, m.Config.ValidationKeyGetter)
	// Check if there was an error in parsing...
	if err != nil {
		m.Config.ErrorHandler(ctx, "令牌过期")
		return fmt.Errorf("Error parsing token2: %v", err)
	}
	if m.Config.SigningMethod != nil && m.Config.SigningMethod.Alg() != parsedToken.Header["alg"] {
		message := fmt.Sprintf("Expected %s signing method but token specified %s",
			m.Config.SigningMethod.Alg(),
			parsedToken.Header["alg"])
		m.Config.ErrorHandler(ctx, message) // 算法错误
		return fmt.Errorf("%s", message)
	}
	// Check if the parsed token is valid...
	if !parsedToken.Valid {
		errMsg := "令牌无效"
		m.Config.ErrorHandler(ctx, errMsg)
		return fmt.Errorf(errMsg)
	}
	if m.Config.Expiration {
		if claims, ok := parsedToken.Claims.(jwt.MapClaims); ok {
			if expired := claims.VerifyExpiresAt(time.Now().Unix(), true); !expired {
				return fmt.Errorf("令牌过期")
			}
		}
	}
	// If we get here, everything worked and we can set the
	// user property in context.
	ctx.Values().Set(m.Config.ContextKey, parsedToken)
	return nil
}

// OnError default error handler
//func OnError(ctx context.Context, err string) {
//	supports.Error(ctx, iris.StatusUnauthorized, supports.Token_Failur, nil)
//}
// jwt中间件配置
func ConfigJWT() {
	if jwts != nil {
		return
	}
	lock.Lock()
	defer lock.Unlock()
	if jwts != nil {
		return
	}
	c := Config{
		ContextKey: conf().Simple.TokenKey,
		//这个方法将验证jwt的token
		ValidationKeyGetter: func(token *jwt.Token) (interface{}, error) {
			//自己加密的秘钥或者说盐值
			return []byte(conf().Simple.TokenKey), nil
		},
		//设置后，中间件会验证令牌是否使用特定的签名算法进行签名
		//如果签名方法不是常量，则可以使用ValidationKeyGetter回调来实现其他检查
		//重要的是要避免此处的安全问题：https://auth0.com/blog/2015/03/31/critical-vulnerabilities-in-json-web-token-libraries/
		//加密的方式
		SigningMethod: jwt.SigningMethodHS256,
		//验证未通过错误处理方式
		ErrorHandler: func(ctx *context.Context, errMsg string) {
			_, _ = ctx.JSON(simple.Unauthorized(errMsg, nil))
		},
		// 指定func用于提取请求中的token
		Extractor: FromAuthCookie,
		// if the token was expired, expiration error will be returned
		Expiration:          true,
		Debug:               true,
		EnableAuthOnOptions: false,
	}
	jwts = &Jwts{Config: c}
}

type Claims struct {
	Id   string `json:"id"`
	Info string `json:"info"`
	jwt.StandardClaims
}

// 在登录成功的时候生成token
func GenerateToken(sign, info string) (string, error) {
	expire := time.Now().Add(time.Duration(conf().Simple.TokenSecond) * time.Second)
	claims := Claims{
		sign,
		info,
		jwt.StandardClaims{
			ExpiresAt: expire.Unix(),
			Issuer:    "iris-jwt",
		},
	}
	tokenClaims := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	token, err := tokenClaims.SignedString([]byte(conf().Simple.TokenKey))
	return token, err
}
func conf() *config.Config {
	return config.Instance()
}
